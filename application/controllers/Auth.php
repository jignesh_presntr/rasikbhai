<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Auth extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model("Appmodel", "app_model");
        $this->load->library('form_validation');
        
    }

    function index()
    {
        if ($this->session->userdata('rasikbhai_is_logged_in')) {
            set_page('dashboard');
        } else {
            redirect('/auth/login/');
        }
    }

    /**
     * Login user on the site
     *
     * @return void
     */
    function login()
    {
        if ($this->session->userdata('rasikbhai_is_logged_in')) {                                    // logged in
            redirect('');
        } else {
            $this->form_validation->set_rules('email','email', 'trim|required|valid_email');
            $this->form_validation->set_rules('pass', 'password', 'trim|required');
            $this->form_validation->set_rules('remember', 'Remember me', 'integer');
            $data['errors'] = array();
            if ($this->form_validation->run()) {
                $email = $_POST['email'];
                $pass = $_POST['pass'];
                $response = $this->app_model->login($email,$pass);
                if ($response) {
                    $this->session->set_userdata('rasikbhai_is_logged_in',$response[0]);
                    $this->session->set_flashdata('success',true);
                    $this->session->set_flashdata('message','You have successfully login.');
                    redirect('');
                } else {
                    $data['errors']['invalid'] = 'Invalid email or password!';
                }
            } else {
                if (validation_errors()) {
                    $error_messages = $this->form_validation->error_array();
                    $data['errors'] = $error_messages;
                }
            }
            $this->load->view('login_form', $data);
        }
    }
    
    function logout()
    {
        $this->session->unset_userdata('rasikbhai_is_logged_in');
        session_destroy();
        redirect('auth/login');
    }

    
    function profile()
    {
        if(!$this->session->userdata('rasikbhai_is_logged_in')) {                                    // logged in
            redirect('');
        }
        $data = array();
        $query = $this->db->get_where('admin',array('admin_id',$this->session->userdata('rasikbhai_is_logged_in')['admin_id']));

        set_page('change_password',$query->row());
        
    }
    
    function change_password()
    {
        $data = array();
        if (!empty($_POST)) {
            $this->form_validation->set_rules('admin_id', 'Admin ID', 'trim|required');
            $this->form_validation->set_rules('old_pass', 'old password', 'trim|required|callback_check_old_password');
            $this->form_validation->set_rules('new_pass', 'new password', 'trim|required');
            $this->form_validation->set_rules('confirm_pass', 'confirm Password', 'trim|required|matches[new_pass]');
            if ($this->form_validation->run()) {
                $this->db->where('admin_id',$_POST['admin_id']);
                $this->db->update('admin',array('admin_password'=>md5($_POST['new_pass'])));
                $this->session->set_flashdata('success',true);
                $this->session->set_flashdata('message','You have successfully changed password!');
                redirect('auth/profile');
            } else {
                if (validation_errors()) {
                    $error_messages = $this->form_validation->error_array();
                    $data['errors'] = $error_messages;
                }
            }
            set_page('change_password',$data);
        } else {
            redirect('auth/profile/');
        }
    }

    function check_old_password($old_pass){
        $admin_id = $_POST['admin_id'];
        $query = $this->db->get_where('admin',array('admin_id'=>$admin_id,'admin_password'=>md5($old_pass)));
        if($query->num_rows() > 0){
            return true;
        }else{
            $this->form_validation->set_message('check_old_password', 'wrong old password.');
            return false;
        }
    }

}
