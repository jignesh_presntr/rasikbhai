<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Group extends CI_Controller
{
    function __construct(){
        parent::__construct();
        $this->is_logged_in();
        $this->load->model("Appmodel", "app_model");
        $this->load->model('Crud', 'crud');
        $this->load->library('form_validation');
    }
    function is_logged_in(){
        if ($this->session->userdata('rasikbhai_is_logged_in')) {
            return true;
        } else {
            redirect('/auth/login/');
        }
    }

    function index(){
        if ($this->session->userdata('rasikbhai_is_logged_in')) {
            set_page('dashboard');
        } else {
            redirect('/auth/login/');
        }
    }
    
    function create_group(){
        
        $data = array(
            'groups' => $this->crud->get_select_data('groups')
        );
        set_page('group',$data);
    }
    function add_group()
    {
        if($this->input->post()){
            $post_data = $this->input->post();
            
            $group_name = $post_data['group_name'];
            $group_print_name = $post_data['group_print_name'];
            $under_group = $post_data['under_group'];
            
            $data = array();
            
            $data['group_name'] = $group_name;
            $data['group_print_name'] = $group_print_name;
            if($under_group == ''){
                $under_group = null;
            }
            $data['parent_group'] = $under_group;
            $data['create_date'] = date("Y-m-d H:i:s");
            
            $insert = $this->crud->insert("groups", $data);
            if($insert){
                $this->session->set_flashdata('success',true);
                $this->session->set_flashdata('message','Group Created Successfully');    
            }
            $status = 1;
            $msg = "Group Created Successfully!";
            echo json_encode(array("status" => $status,"msg" => $msg));
            redirect('group/create_group');
            exit;
        }
    }
    
    function delete_group($group_id){
        $this->crud->delete('groups', array('group_id' => $group_id));
		$this->session->set_flashdata('success',true);
        $this->session->set_flashdata('message','Group Deleted Successfully');
        redirect('group/create_group');
        exit;
        
    }
    
}