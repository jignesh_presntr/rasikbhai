<?php 
class Crud extends CI_Model
{
	function __construct()
	{
		parent::__construct();
	}

	/**
	 * @param $table_name
	 * @param $data_array
	 * @return bool
	 */
	function insert($table_name,$data_array){
		if($this->db->insert($table_name,$data_array))
		{
			return $this->db->insert_id();
		}
		return false;
	}
        
        function insertFromSql($sql)
        {
            $this->db->query($sql);
            return $this->db->insert_id();
        }

        function execuetSQL($sql){
            $this->db->query($sql);
        }
	function getFromSQL($sql)
        {
		return $this->db->query($sql)->result();
	}

	/**
	 * @param $table_name
	 * @param $order_by_column
	 * @param $order_by_value
	 * @return bool
	 */
	function get_all_records($table_name,$order_by_column,$order_by_value){
		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->order_by($order_by_column,$order_by_value);
		$query = $this->db->get();
		if ($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	/**
	 * @param $table_name
	 * @param $order_by_column
	 * @param $order_by_value
	 * @param $where_array
	 * @return bool
	 */
	function get_all_with_where($table_name,$order_by_column,$order_by_value,$where_array){
		
		// echo "<pre>";print_r($where_array);exit;

		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->where($where_array);
		$this->db->order_by($order_by_column,$order_by_value);
		$query = $this->db->get();
		//echo $this->db->last_query();exit;
		if ($query->num_rows() > 0){
			return $query->result();
		}else{
			return false;
		}
	}

	/**
	 * @param $tbl_name
	 * @param $column_name
	 * @param $where_id
	 * @return mixed
	 */
	function get_column_value_by_id($tbl_name,$column_name,$where_id)
	{				
		$this->db->select("*");
		$this->db->from($tbl_name);
		$this->db->where($where_id);		
		$this->db->last_query();
		$query = $this->db->get();
		return $query->row($column_name);
	}

	/**
	 * @param $table_name
	 * @param $where_id
	 * @return mixed
	 */
	function get_row_by_id($table_name,$where_id){
		$this->db->select("*");
		$this->db->from($table_name);
		$this->db->where($where_id);
		$query = $this->db->get();
		return $query->result();
	}

	/**
	 * @param $table_name
	 * @param $where_array
	 * @return mixed
	 */
	function delete($table_name,$where_array){		
		$result = $this->db->delete($table_name,$where_array);
		return $result;
	}

	/**
	 * @param $table_name
	 * @param $data_array
	 * @param $where_array
	 * @return mixed
	 */
	function update($table_name,$data_array,$where_array){
		$this->db->where($where_array);
		$rs = $this->db->update($table_name, $data_array);
		return $rs;
	}

	/**
	 * @param $name
	 * @param $path
	 * @return bool
	 */
	function upload_file($name, $path)
	{
		$config['upload_path'] = $path;
		$config ['allowed_types'] = '*';
		$this->upload->initialize($config);
		if($this->upload->do_upload($name))
		{
			$upload_data = $this->upload->data();
			return $upload_data['file_name'];
		}
		return false;
	}

	
	/**
	 * @param $table
	 * @param $id_column
	 * @param $column
	 * @param $column_val
	 * @return null
	 */
	function get_id_by_val($table,$id_column,$column,$column_val){
		$this->db->select($id_column);
		$this->db->from($table);
		$this->db->where($column,$column_val);
		$this->db->limit('1');
		$query = $this->db->get();
		if($query->num_rows() > 0){
			return $query->row()->$id_column;
		} else {
			return null;
		}
	}
	
	
	function limit_words($string, $word_limit=30){
		$words = explode(" ",$string);
		return implode(" ", array_splice($words, 0, $word_limit));
	}
	function limit_character($string, $character_limit=30){
		if (strlen($string) > $character_limit) {
			return substr($string, 0, $character_limit).'...';
		}else{
			return $string;
		}
	}
    //select data 
    function get_select_data($tbl_name)
	{
		$this->db->select("*");
		$this->db->from($tbl_name);
		$query = $this->db->get();
		return $query->result();
	}
    function get_data_row_by_id($tbl_name,$where,$where_id)
	{
		$this->db->select("*");
		$this->db->from($tbl_name);
		$this->db->where($where,$where_id);
		$query = $this->db->get();
		return $query->row();
	}
    function get_result_where($tbl_name,$where,$where_id)
	{
		$this->db->select("*");
		$this->db->from($tbl_name);
		$this->db->where($where,$where_id);
		$query = $this->db->get();
		return $query->result();
	}
    function get_all_cat($table,$id,$name){
        $query = $this->db->query("SELECT `" . $id . "`  as id,`" . $name . "` as text FROM `" . $table . "` ORDER BY `" . $name . "` ASC");
        return $query->result();
    }
    function get_duplicate($tbl_name,$where)
	{
		$this->db->select("*");
		$this->db->from($tbl_name);
		$this->db->where($where);
		$query = $this->db->get();
		return $query->row();
	}
    public function product_list()
    {			
        $this->db->select('*');
        $this->db->from('product');
        $this->db->join('category', 'category.category_id = product.category_id');
        $this->db->join('item', 'item.item_id = product.item_id');
        $this->db->join('company', 'company.company_id = product.company_id');
        $this->db->join('variant', 'variant.variant_id = product.variant_id');
        $query = $this->db->get();
        return $query->result();            
    }
    public function planning_list()
    {			
        $this->db->select('*');
        $this->db->from('planning');
        $this->db->join('admin', 'admin.admin_id = planning.customer_admin_id');
        $query = $this->db->get();
        return $query->result();            
    }
    
    public function complain_list()
    {			
        $this->db->select('admin.admin_name, category.category_name, item.item_name, company.company_name, variant.variant_name, complain.product_image, complain.remark, complain.create_date');
        $this->db->from('complain');
        $this->db->join('admin', 'admin.admin_id = complain.employee_admin_id' , 'admin.admin_id = complain.customer_admin_id');
        $this->db->join('product', 'product.product_id = complain.product_id');
        $this->db->join('category', 'product.category_id = category.category_id');
        $this->db->join('item', 'product.item_id = item.item_id');
        $this->db->join('company', 'product.company_id = company.company_id');
        $this->db->join('variant', 'product.variant_id = variant.variant_id');
        $query = $this->db->get();
        return $query->result();            
    }
    
    
}
