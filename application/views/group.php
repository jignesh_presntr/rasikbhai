<!-- /.content-wrapper -->  
<div class="content-wrapper">
    <section class="content-header">
      <h1> Group : Creation
      </h1>
    </section>
    <!-- Main content -->
    <section class="content">
    <div class="row">
        <div class="col-md-12 display_alert">
            <?php
                $this->load->view('success_false_notify');
            ?>
        </div>
    </div>
      <div class="row">
        <div class="col-md-12">
            <div class="box box-primary">
            <div class="box-body table-responsive">
                <?=form_open('group/add_group' , array('id'=>'','method'=>'post','enctype'=>'multipart/form-data','data-parsley-validate novalidate'));?>
                    <div class="col-md-4">
                        <div class="form-group">
                            <?= form_label('Group Name','',array('class'=>'control-labelsm','for' => 'group_name'));?>
                            <?=form_input(array('type' => 'text','name' => 'group_name','id' => 'group_name','placeholder' => 'Group Name', 'class' => 'form-control input-sm','autofocus' => 'autofocus','required'=>'required'));?>
                        </div>
                        <div class="form-group">
                            <?= form_label('Group Print Name','',array('class'=>'control-labelsm','for' => 'group_print_name'));?>
                            <?=form_input(array('type' => 'text','name' => 'group_print_name','id' => 'group_print_name','placeholder' => 'Group Print Name', 'class' => 'form-control input-sm'));?>
                        </div>
                        
                        <div class="form-group">
                            
                            <?= form_label('Under Group','',array('class'=>'control-labelsm','for' => 'under_group'));?>
                            <select name="under_group" id="under_group" class="select2" style="width:100%">
                                <option value=""> - Select Group - </option>
                                <?php 
                                foreach($groups as $group){ 
                                    echo '<option value="'.$group->group_id.'" > '.$group->group_name.'</option>';
                                } 
                                ?>
                            </select>
                        </div>
                        <div class="form-group text-left m-b-0">
                            <?= form_submit(array('id' => '', 'type' => 'submit', 'class' => 'btn btn-primary waves-effect waves-light', 'value' => 'Submit')); ?>
                        </div>
                    </div>
                <?=form_close();?>
                <div class="col-md-8">
                    <div class="box ">
                    <form id="frm-example" action="" method="POST">
                    <div class="box-body table-responsive">
                        <table id="datatable" class="table table-striped table-bordered display select" >
                            <thead>
                                <tr>
                                    <th>Action</th>
                                    <th>Group Id</th>
                                    <th>Group Name</th>
                                    <th>Group Print Name</th>
                                    <th>Under Group</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach($groups as $group)
                                {
                                ?>
                                <tr>
                                    <td>
                                        <!--<a class="btn btn-xs btn-primary">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                        <a class="btn btn-xs btn-danger">
                                            <i class="fa fa-trash"></i>
                                        </a>-->
                                        
                                        <!--<a href="" onclick="return confirm('are you sure?')"  class="btn waves-effect waves-light btn-default btn-xs text-danger" style="margin-left:10px" title="Click to Delete Party"><i class="fa fa-trash"> </i></a> 
                                        <a href="" class="btn waves-effect waves-light btn-default btn-xs text-info" style="margin-left:10px" title="Click to Edit Party"><i class="fa fa-edit"> </i></a> -->
                                        <a href="<?=base_url('group/delete_group/'.$group->group_id);?>" onclick="return confirm('are you sure?')"  class="btn waves-effect waves-light btn-default btn-xs text-danger" style="margin-left:10px" title="Click to Delete Group"><i class="fa fa-trash"> </i></a> 
                                        
                                    </td>
                                    <td><?=$group->group_id; ?></td>
                                    <td><?=$group->group_name; ?></td>
                                    <td><?=$group->group_print_name; ?></td>
                                    <td>
                                    <?php
                                        $g_id = $group->parent_group;
                                        echo $this->crud->get_id_by_val('groups','group_name','group_id',$g_id);
                                    ?>     
                                    </td>
                                    
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    </form>
                </div>
                    
                </div>
            </div>
            </div>
        </div>
    </div>
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<script src="<?=base_url('assets/plugins/select2/select2.min.js');?>"></script>
<script type="text/javascript">
$(document).ready(function(){
    $('.select2').select2();
    $("#datatable").dataTable();
    var base_url = "<?php echo base_url();?>";
    $(document).on('submit', '#main-frm', function(){
        $.ajax({
            type: 'post',
            url: '<?=base_url();?>group/add_group',
            data: $('#main-frm').serialize(),
            success: function(data) {
                var data = JSON.parse(data);
                $msg = data.msg;
                if(data.status == 1)
                {
                    $("#main-frm")[0].reset();
                    //$("#main-frm").trigger('reset');
                    $('#select2-chosen-1').html(' - Select Customer - ');
                    $('#select2-chosen-2').html(' - Select Product - ');
                    show_notify($msg,true);
                }
                else
                {
                    show_notify($msg,false);
                }                    
            }
        });          
        return false;
   });
    
});
</script>