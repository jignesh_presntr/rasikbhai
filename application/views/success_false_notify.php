<?php if($this->session->flashdata('success') === true  && $this->session->flashdata('message') != '') { ?>
    <div class="col-sm-41 pull-right1 alert-div" style="z-index: 999;padding-right: 0;">
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> <?= $this->session->flashdata('message') ?></h4>
        </div>
    </div>
    <?php
}
if($this->session->flashdata('success') === false  && $this->session->flashdata('message') != '') {
    ?>
    <div class="col-sm-41 pull-right1 alert-div" style="z-index: 999;padding-right: 0;">
        <div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
            <h4><i class="icon fa fa-check"></i> <?= $this->session->flashdata('message') ?></h4>
        </div>
    </div>
    <?php
}?>