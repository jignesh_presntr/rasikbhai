<!-- Left side column. contains the logo and sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel">
        <div class="pull-left image">
          <img src="<?php echo base_url('assets/dist/img/user2-160x160.jpg');?>" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p><?php echo isset($this->session->userdata("rasikbhai_is_logged_in")['admin_lname'])?ucwords($this->session->userdata("rasikbhai_is_logged_in")['admin_lname']):'Admin';?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
        </div>
      </div>
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
        <li class="active treeview">
          <a href="<?=base_url();?>">
            <i class="fa fa-dashboard"></i> <span>Dashboard</span>
          </a>
        </li>
        <!--<li class="treeview">
          <a href="#">
            <i class="fa fa-user"></i> <span> aaa</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
            <li><a href="#"><i class="fa fa-circle-o"></i> Add </a></li>
            <li><a href="#"><i class="fa fa-list-ol"></i> List</a></li>
          </ul>
        </li>-->
          
        <li>
            <a href="<?=base_url('group/create_group')?>">
                <i class="fa fa-user"></i> <span>Create Group</span>
            </a>
        </li>
        
        
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>