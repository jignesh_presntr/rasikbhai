<?php 
    $user_loginarry = $this->session->userdata("rasikbhai_is_logged_in");
    
    //print_r($name = $this->session->userdata('admin_lname')); exit;
    $login_admin_name = $user_loginarry['admin_lname'];
    $login_admin_email = $user_loginarry['admin_email'];
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title> Rasikbhai | Dashboard</title>
    <!-- Tell the browser to be responsive to screen width -->
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <!-- Bootstrap 3.3.6 -->
    <link rel="stylesheet" href="<?= base_url();?>assets/bootstrap/css/bootstrap.min.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
    <!-- Ionicons -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
    <!-- jvectormap -->
    <link rel="stylesheet" href="<?= base_url('assets/plugins/jvectormap/jquery-jvectormap-1.2.2.css');?>">


    <!----------------Notify---------------->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/notify/jquery.growl.css');?>">
    <!----------------Notify---------------->

    <!-- Parsley -->
    <link rel="stylesheet" href="<?= base_url();?>assets/plugins/parsleyjs/src/parsley.css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <!-- Select2 -->
    <link rel="stylesheet" href="<?= base_url();?>assets/plugins/select2/select2.min.css">
    <!-- bootstrap datepicker -->
    <link rel="stylesheet" href="<?= base_url();?>assets/plugins/datepicker/datepicker3.css">

    <!-- jQuery 2.2.3 -->
    <script src="<?= base_url('assets/plugins/jQuery/jquery-2.2.3.min.js');?>"></script>
    
    
    
    <!-- DataTables -->
    <link rel="stylesheet" href="<?=base_url('assets/plugins/datatables/dataTables.bootstrap.css');?>" type="text/css">
    <script src="<?=base_url('assets/plugins/datatables/jquery.dataTables.min.js');?>"></script>
    <script src="<?=base_url('assets/plugins/datatables/dataTables.bootstrap.min.js');?>"></script>
    
    <!-- Theme style -->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/AdminLTE.min.css');?>">
    <!-- AdminLTE Skins. Choose a skin from the css/skins
    folder instead of downloading all of them to reduce the load. -->
    <link rel="stylesheet" href="<?= base_url('assets/dist/css/skins/_all-skins.min.css');?>">
    <link href="<?= base_url('assets/plugins/sweetalert/dist/sweetalert.css');?>" rel="stylesheet" type="text/css">
    <link href="<?= base_url('assets/dist/css/custom.css');?>" rel="stylesheet" type="text/css" />
    
</head>
<body class="hold-transition skin-blue sidebar-mini">
<div class="wrapper">
  <header class="main-header">
    <!-- Logo -->
    <a href="<?= base_url()?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><b>R</b></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>Rasikbhai</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <!-- Navbar Right Menu -->
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="<?php echo base_url('assets/dist/img/user2-160x160.jpg');?>" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo isset($login_admin_name)?ucwords($login_admin_name):'Admin';?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
                <img src="<?php echo base_url('assets/dist/img/user2-160x160.jpg');?>" class="img-circle" alt="User Image">
                <p>
                    <?php echo isset($login_admin_name)?ucwords($login_admin_name):'Admin';?>
                    <br/>
                    <?php echo isset($login_admin_email)?$login_admin_email:'';?>
                </p>
                
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <a href="<?= base_url()?>auth/profile" class="btn btn-default btn-flat">Change Password</a>
                </div>
                <div class="pull-right">
                  <a href="<?= base_url()?>auth/logout" class="btn btn-default btn-flat">Sign out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
        </ul>
      </div>
    </nav>
  </header>