-- phpMyAdmin SQL Dump
-- version 4.6.6
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Feb 23, 2017 at 03:10 PM
-- Server version: 5.7.17-0ubuntu0.16.10.1
-- PHP Version: 7.0.15-0ubuntu0.16.10.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `rasikbhai`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `admin_id` int(11) NOT NULL,
  `admin_fname` varchar(22) NOT NULL,
  `admin_lname` varchar(22) NOT NULL,
  `admin_email` varchar(255) NOT NULL,
  `admin_img` varchar(255) NOT NULL,
  `admin_password` varchar(222) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`admin_id`, `admin_fname`, `admin_lname`, `admin_email`, `admin_img`, `admin_password`) VALUES
(1, 'main', 'admin 1', 'admin@gmail.com', '', '75d23af433e0cea4c0e45a56dba18b30');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

CREATE TABLE `groups` (
  `group_id` int(11) NOT NULL,
  `group_name` varchar(222) NOT NULL,
  `group_print_name` varchar(222) NOT NULL,
  `parent_group` int(11) DEFAULT NULL,
  `create_date` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`group_id`, `group_name`, `group_print_name`, `parent_group`, `create_date`) VALUES
(2, 'Primary', 'Primary', NULL, '2017-02-23 15:01:26'),
(3, 'Capital Account', 'Capital Account', 2, '2017-02-23 15:02:00'),
(4, 'Loan [Responsibility]', 'Loan [Responsibility]', 2, '2017-02-23 15:02:31'),
(5, 'Current Responsibility', 'Current Responsibility', 2, '2017-02-23 15:02:39'),
(6, 'Fixed Assets', 'Fixed Assets', 2, '2017-02-23 15:02:50'),
(7, 'Investments', 'Investments', 2, '2017-02-23 15:02:58'),
(8, 'Current Assets', 'Current Assets', 2, '2017-02-23 15:03:05'),
(9, 'Branch & Division', 'Branch & Division', 2, '2017-02-23 15:03:13'),
(10, 'Suspense Account', 'Suspense Account', 2, '2017-02-23 15:03:22'),
(11, 'Sales Account', 'Sales Account', 2, '2017-02-23 15:03:29'),
(12, 'Purchase Account', 'Purchase Account', 2, '2017-02-23 15:03:36'),
(13, 'Direct Incomes', 'Direct Incomes', 2, '2017-02-23 15:03:44'),
(14, 'Direct Expences', 'Direct Expences', 2, '2017-02-23 15:03:53'),
(15, 'Indirect Incomes', 'Indirect Incomes', 2, '2017-02-23 15:04:02'),
(16, 'indirect Expences', 'indirect Expences', 2, '2017-02-23 15:04:09'),
(17, 'Misc. Expences [Assets]', 'Misc. Expences [Assets]', 2, '2017-02-23 15:04:16'),
(18, 'Reserve & Surplus', 'Reserve & Surplus', 3, '2017-02-23 15:04:40'),
(19, 'Secured Loan', 'Secured Loan', 4, '2017-02-23 15:04:58'),
(20, 'UnSecured Loan', 'UnSecured Loan', 4, '2017-02-23 15:07:19'),
(21, 'Bank Over Draft Ac.', 'Bank Over Draft Ac.', 4, '2017-02-23 15:07:36'),
(22, 'Bank Over Cash Credit', 'Bank Over Cash Credit', 4, '2017-02-23 15:07:46'),
(23, 'Duties & Taxes', 'Duties & Taxes', 5, '2017-02-23 15:07:58'),
(24, 'Provisions', 'Provisions', 5, '2017-02-23 15:08:08'),
(25, 'Loan & Advance [Assets]', 'Loan & Advance [Assets]', 8, '2017-02-23 15:08:24'),
(26, 'Stock On Hand', 'Stock On Hand', 8, '2017-02-23 15:08:36'),
(27, 'Sundry Debtors', 'Sundry Debtors', 8, '2017-02-23 15:08:46'),
(28, 'Cash On Hand', 'Cash On Hand', 8, '2017-02-23 15:08:55'),
(29, 'Bank Account', 'Bank Account', 8, '2017-02-23 15:09:03'),
(30, 'Sales Order Advance `', 'Sales Order Advance `', 5, '2017-02-23 15:09:17');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`group_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin`
--
ALTER TABLE `admin`
  MODIFY `admin_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `group_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
